FROM node:18

RUN apt-get update && apt-get install -y git
RUN npm install -g pm2 nodemon

WORKDIR /usr/src/app/backend
COPY backend/package*.json ./
RUN npm install
COPY backend/ ./

WORKDIR /usr/src/app/frontend
COPY frontend/package*.json ./
RUN npm install
COPY frontend/ ./

WORKDIR /usr/src/app
COPY . .

EXPOSE 3080 8080

CMD ["tail", "-f", "/dev/null"]


